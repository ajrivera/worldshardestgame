﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_projectile : MonoBehaviour
{
    public int mode;
    // Start is called before the first frame update
    void Start()
    {
        switch (mode){
            case 1:
                //this.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right*2, ForceMode2D.Impulse);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(2f, 0f);
                break;
            case 2:
                //this.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left*2, ForceMode2D.Impulse);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-2f, 0f);
                break;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.name=="player")
            switch (mode)
            {
                case 1:
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(2f, 0f);
                    break;
                case 2:
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(-2f, 0f);
                    break;
            }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
