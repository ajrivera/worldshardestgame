﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class scr_player : Agent
{

    private Vector3 posInicial;
    public Transform posFinal;

    private Vector3 posActual;
    private Vector3 posPrev;
    private Vector3 posyokese;

    public GameObject[] checks;
    //public delegate void ResetDelegate();
    //public event ResetDelegate OnReset;
    public override void Initialize()
    {
        base.Initialize();
        posInicial = this.transform.localPosition;
        //posyokese = posInicial;
        posActual = this.transform.localPosition;
        posPrev = posActual;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(posFinal.localPosition - posInicial);
        sensor.AddObservation(posFinal.localPosition);
    }
    private void Start()
    {
        InvokeRepeating("checkdistance", 0f, 2f);
    }
    private void Update()
    {

        posActual = this.transform.localPosition;
        if (((posFinal.localPosition.x - posActual.x) < (posFinal.localPosition.x - posPrev.x)) || ((posFinal.localPosition.y - posActual.y) < (posFinal.localPosition.y - posPrev.y)))
        {
            AddReward(0.1f);
            posPrev = posActual;
        }
        else
        {
            AddReward(-0.2f);
        }
        //if ((posFinal.localPosition.x-posActual.x < posFinal.localPosition.x - posPrev.x) || (posFinal.localPosition.y - posActual.y < posFinal.localPosition.y - posPrev.y))
        //{
        //    AddReward(0.1f);
        //}
    }


    public override void OnActionReceived(ActionBuffers actions)
    {
        float v1 = actions.DiscreteActions[0];
        float v2 = actions.DiscreteActions[1];
        v1--;
        v2--;
        transform.localPosition += new Vector3(v1 / 20f, v2 / 20f);

    }

    public override void OnEpisodeBegin()
    {
        this.transform.localPosition = posInicial;
        posActual = this.transform.localPosition;
        posPrev = posActual;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        foreach (GameObject check in checks)
        {
            check.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "proj")
        {
            AddReward(-30f);

            EndEpisode();
        }
        if (collision.transform.tag == "wall")
        {
            AddReward(-1f);
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "win")
        {
            AddReward(15f);
            print("----GOT'EM----");
            collision.enabled = false;
            if (collision.transform.name == "final")
            {
                collision.enabled = true;
                EndEpisode();
            }
        }
    }

    private void checkdistance()
    {
        //if (((posFinal.localPosition.x - posActual.x) < (posFinal.localPosition.x - posPrev.x)) || ((posFinal.localPosition.y - posActual.y) < (posFinal.localPosition.y - posPrev.y)))
        //    AddReward(0.1f);
        //    posyokese = posPrev;
        //if(((posInicial.x - posActual.x) < (posInicial.x - posyokese.x)) || ((posInicial.y - posActual.y) < (posInicial.y - posyokese.y)))
        //    posPrev = posActual;

        print(GetCumulativeReward());
    }

}
